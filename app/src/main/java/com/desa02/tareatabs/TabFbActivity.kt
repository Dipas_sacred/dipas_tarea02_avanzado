package com.desa02.tareatabs

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.desa02.tareatabs.R
import com.desa02.tareatabs.fragment.HomeFragment
import com.desa02.tareatabs.fragment.NotificacionesFragment
import com.desa02.tareatabs.fragment.PaginasFragment
import kotlinx.android.synthetic.main.activity_tab_fb.*

class TabFbActivity : AppCompatActivity(), View.OnClickListener {

    private val FRAGMENT_HOME = 0
    private val FRAGMENT_PAGINAS = 1
    private val FRAGMENT_NOTIFICACIONES = 2

    private val indicatorViews= mutableListOf<View>()
    private val titleViews= mutableListOf<TextView>()
    private var currentIndicator = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_fb)

        indicatorViews.add(vi_home)
        indicatorViews.add(vi_paginas)
        indicatorViews.add(vi_notificaciones)

        titleViews.add(tv_home)
        titleViews.add(tv_paginas)
        titleViews.add(tv_notificaciones)

        titleViews.forEach {
            it.setOnClickListener(this)
        }

        selectFirst()
    }

    override fun onClick(view: View?) {
        val bundle = Bundle()

        var fragmentId = when(view?.id){
            R.id.tv_home->  FRAGMENT_HOME
            R.id.tv_paginas->  FRAGMENT_PAGINAS
            R.id.tv_notificaciones->  FRAGMENT_NOTIFICACIONES

            else -> FRAGMENT_HOME
        }
        
        updateUI(fragmentId);
        changeFragment(bundle,fragmentId)
    }

    private fun updateUI(fragmentId: Int) {
        if (currentIndicator >= 0) {
            indicatorViews[currentIndicator].setBackgroundColor(Color.parseColor("#201F50A0"))
            titleViews[currentIndicator].setTypeface( null , Typeface.NORMAL )
        }
        indicatorViews[fragmentId].setBackgroundColor(Color.parseColor("#3b5998"))
        titleViews[fragmentId].setTypeface( Typeface.DEFAULT_BOLD )
        currentIndicator = fragmentId
    }

    private fun changeFragment(bundle: Bundle, fragmentId: Int) {

        val fragment = factoryFragment(fragmentId)
        //fragment.setArguments(bundle);

        fragment?.let {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flayContainer, it)
                addToBackStack(null)
                commit()
            }
        }
    }

    private fun factoryFragment( fragmentId: Int): Fragment? {
        when (fragmentId) {
            FRAGMENT_HOME -> return HomeFragment()
            FRAGMENT_PAGINAS -> return PaginasFragment()
            FRAGMENT_NOTIFICACIONES -> return NotificacionesFragment()
        }
        return null
    }
    private fun selectFirst() {
        val bundle = Bundle()
        val fragmentId = FRAGMENT_HOME
        updateUI(fragmentId)
        changeFragment(bundle, fragmentId)
    }




}